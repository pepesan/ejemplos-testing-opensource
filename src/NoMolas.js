import { html, css, LitElement } from 'lit';

export class NoMolas extends LitElement {
  static styles = css`
    :host {
      display: block;
      padding: 25px;
      color: var(--no-molas-text-color, #000);
    }
  `;

  static properties = {
    header: { type: String },
    counter: { type: Number },
  };
  /*
      Función constructora
  */
  constructor() {
    super();
    this.header = 'Hey there';
    this.counter = 5;
  }
  /*
    Nueva función que incrementa el counter en 1
   */
  __increment() {
    console.log("increment");
    this.counter += 1;
  }
  /*
    Nueva función que decrementa el counter en 1
   */

  __decrement() {
    console.log("decrement");
    this.counter -= 1;
  }
  /*
    Nueva función que resetea el counter a 0
   */
  __reset(){
    this.counter=0;
  }
  /*
    Coloca un valor predefinido a 10
   */
  __a_10(){
    this.counter = 10;
  }

  render() {
    return html`
      <h2 id="titulo-to-molon">${this.header} Nr. ${this.counter}!</h2>
      <button id="boton-to-chulo" @click=${this.__increment}>Increment</button>
      <button id="boton-to-chulo-decrementor" @click=${this.__decrement}>Decrement</button>
      <button id="button-nomolas-reset" @click=${this.__reset}>Reset</button>
      <button id="boton-to-chulo-a-diez" @click="${this.__a_10}"> A 10 </button>
    `;
  }
}
