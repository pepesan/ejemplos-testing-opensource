import { html } from 'lit';
import { fixture, expect , assert } from '@open-wc/testing';

import '../no-molas.js';

describe('NoMolas', () => {
  it('Checking the constructor function has a default header "Hey there" and counter 5', async () => {
    const el = await fixture(html`<no-molas></no-molas>`);

    expect(el.header).to.equal('Hey there');
    // expect(el.counter).to.equal(5);
    assert.equal(el.counter, 5, "No es cinco")
  });

  it('increases the counter on button click', async () => {
    const el = await fixture(html`<no-molas></no-molas>`);
    expect(el.counter).to.equal(5);
    el.shadowRoot.querySelector('#boton-to-chulo').click();
    expect(el.counter).to.equal(6);
  });
  it('decrease the counter on button click', async () => {
    const el = await fixture(html`<no-molas></no-molas>`);
    expect(el.counter).to.equal(5);
    el.shadowRoot.querySelector('#boton-to-chulo-decrementor').click();
    expect(el.counter).to.equal(4);
  });
  it('put a 10 into the counter on button click', async () => {
    const el = await fixture(html`<no-molas></no-molas>`);
    expect(el.counter).to.equal(5);
    el.shadowRoot.querySelector('#boton-to-chulo-a-diez').click();
    expect(el.counter).to.equal(10);
  });
  it('reset the counter on button click', async () => {
    const el = await fixture(html`<no-molas></no-molas>`);
    expect(el.counter).to.equal(5);
    el.shadowRoot.querySelector('#button-nomolas-reset').click();
    expect(el.counter).to.equal(0);
  });

  it('can override the header via attribute', async () => {
    const el = await fixture(html`<no-molas header="attribute header"></no-molas>`);

    expect(el.header).to.equal('attribute header');
  });

  it('passes the a11y audit', async () => {
    const el = await fixture(html`<no-molas></no-molas>`);

    await expect(el).shadowDom.to.be.accessible();
  });
});
