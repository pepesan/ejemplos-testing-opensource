import { html } from 'lit';
import { fixture, expect , assert } from '@open-wc/testing';
import sinon from 'sinon';
import '../no-molas.js';


describe('NoMolas', () => {
  var monitor;
  beforeEach(() =>{
    monitor =sinon.spy(console,'log')
  })
  afterEach(() => {
    monitor.restore();
  });

  it('check the call to console.log into increment', async () => {

    const el = await fixture(html`<no-molas></no-molas>`);
    expect(el.counter).to.equal(5);
    el.shadowRoot.querySelector('#boton-to-chulo').click();
    expect(el.counter).to.equal(6);
    expect(monitor.callCount).to.equal(1);
  });
  it('check the call to console.log intro decrement', async () => {
    const el = await fixture(html`<no-molas></no-molas>`);
    expect(el.counter).to.equal(5);
    el.shadowRoot.querySelector('#boton-to-chulo-decrementor').click();
    expect(el.counter).to.equal(4);
    expect(monitor.callCount).to.equal(1);
  });
  it('check the call to console.log into increment and decrement', async () => {

    const el = await fixture(html`<no-molas></no-molas>`);
    expect(el.counter).to.equal(5);
    el.shadowRoot.querySelector('#boton-to-chulo').click();
    expect(el.counter).to.equal(6);
    el.shadowRoot.querySelector('#boton-to-chulo-decrementor').click();
    expect(el.counter).to.equal(5);
    expect(monitor.callCount).to.equal(2);
  });
});
